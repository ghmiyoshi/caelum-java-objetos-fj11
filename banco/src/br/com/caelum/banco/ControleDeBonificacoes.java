package br.com.caelum.banco;

public class ControleDeBonificacoes {

	private double totalDeBonificacoes;

	public void registra(Funcionario funcionario) {
		System.out.println("Adicionando bonificação do funcionário: " + funcionario);
		this.totalDeBonificacoes += funcionario.getBonificacao();
	}

	public double getTotalDeBonificacoes() {
		return this.totalDeBonificacoes;
	}

}
