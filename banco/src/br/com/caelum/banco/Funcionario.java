package br.com.caelum.banco;

public abstract class Funcionario {

	private String nome;
	private String cpf;
	protected double salario;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	/**
	 * Os funcionários comuns recebem 10% do valor do salário
	 * 
	 * @return Bonificação do funcionário
	 **/
	public double getBonificacao() {
		return getSalario() * 0.10;
	}

	@Override
	public String toString() {
		return this.nome;
	}

}
