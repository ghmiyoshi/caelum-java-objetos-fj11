package br.com.caelum.banco;

public class Gerente extends Funcionario implements Autenticavel {

	private int senha;

	@Override
	public double getBonificacao() {
		return super.getSalario() * 0.15;
	}

	@Override
	public boolean autentica(int senha) {
		if (this.senha != senha) {
			return false;
		} else {
			return true;
		}
	}

	public void setSenha(int senha) {
		this.senha = senha;
	}

}
