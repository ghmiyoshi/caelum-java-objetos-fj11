package br.com.caelum.main;

import br.com.caelum.banco.Autenticavel;
import br.com.caelum.banco.ControleDeBonificacoes;
import br.com.caelum.banco.Gerente;
import br.com.caelum.banco.SistemaInterno;

public class Banco {

	/* 11.5 EXERCÍCIOS: INTERFACES */

	public static void main(String[] args) {
		Gerente gerente = new Gerente();
		Autenticavel gerenteAutenticavel = new Gerente();
		Gerente gerente2 = (Gerente) gerenteAutenticavel;
		ControleDeBonificacoes controleDeBonificacoes = new ControleDeBonificacoes();
		SistemaInterno sistemaInterno = new SistemaInterno();

		gerente.setSalario(5000);
		controleDeBonificacoes.registra(gerente);
		
		System.out.println(gerente.getBonificacao());
		System.out.println(controleDeBonificacoes.getTotalDeBonificacoes());

		gerente2.setSenha(123);
		gerente2.autentica(123);
		sistemaInterno.login(gerenteAutenticavel);
	}

}
