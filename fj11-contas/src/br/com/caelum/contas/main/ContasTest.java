package br.com.caelum.contas.main;

import br.com.caelum.javafx.api.main.SistemaBancario;

/**
 * Inicia o Sistema Bancário
 * 
 * @author Gabriel Hideki Miyoshi
 * @since 20/09/2018
 *
 */
public class ContasTest {

	/* 8.7 EXERCÍCIOS: IMPORTANDO UM JAR 
	   8.9 EXERCÍCIOS: MOSTRANDO OS DADOS DA CONTA NA TELA 
	*/

	public static void main(String[] args) {
		SistemaBancario.mostraTela(false);
		//TelaDeContas.main(args);
	}

}
