package br.com.caelum.contas.main;

import br.com.caelum.javafx.api.main.OlaMundo;

/**
 * Jar Test
 * 
 * @author Gabriel Hideki Miyoshi
 * @since 20/09/2018
 *
 */
public class JarTest {

	/* 8.7 EXERCÍCIOS: IMPORTANDO UM JAR */
	
	public static void main(String[] args) {
		OlaMundo.main(args);
	}

}
