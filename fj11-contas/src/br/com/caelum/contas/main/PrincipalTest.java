package br.com.caelum.contas.main;

import br.com.caelum.contas.modelo.Conta;
import br.com.caelum.contas.modelo.ContaCorrente;

/**
 * Principal Test
 * 
 * @author Gabriel Hideki Miyoshi
 * @since 20/09/2018
 *
 */
public class PrincipalTest {
	
	/* 6.8 EXERCÍCIOS: ECLIPSE 
	   7.6 EXERCÍCIOS: PACOTES
	   8.5 EXERCÍCIOS: JAR E JAVADOC	  
	*/ 

	public static void main(String[] args) {
		Conta conta = new ContaCorrente();
		conta.deposita(100);
		
		System.out.println(conta.getTipo());

		System.out.println(conta.getSaldo());
	}

}