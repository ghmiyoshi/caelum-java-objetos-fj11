package br.com.caelum.main;

public class LambdaTest {

	/* E com lambda do Java 8? */

	public static void main(String[] args) {
		Runnable runnable = () -> {
			for (int i = 0; i < 10000; i++)
				System.out.println("programa 1 valor " + i);
		};
		new Thread(() -> {
			for (int i = 0; i < 10000; i++) {
				System.out.println("programa 1 valor " + i);
			}
		}).start();
	}

}
