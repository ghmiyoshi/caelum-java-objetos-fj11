package br.com.caelum.main;

import br.com.caelum.thread.Programa;

public class ThreadTest {

	/* 18.2 ESCALONADOR E TROCAS DE CONTEXTO */

	public static void main(String[] args) {
		Programa programa = new Programa();
		programa.setId(1);

		Thread thread = new Thread(programa);
		thread.start();

		Programa programa2 = new Programa();
		programa2.setId(2);

		Thread thread2 = new Thread(programa2);
		thread2.start();
	}

}
