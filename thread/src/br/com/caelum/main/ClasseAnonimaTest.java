package br.com.caelum.main;

public class ClasseAnonimaTest {

	/* 18.5 E AS CLASSES ANÔNIMAS? */

	public static void main(String[] args) {
		Runnable runnable = new Runnable() {

			public void run() {
				for (int i = 0; i < 10000; i++)
					System.out.println("programa 1 valor " + i);
			}

		};
		Thread t = new Thread(runnable);
		t.start();
	}

}
