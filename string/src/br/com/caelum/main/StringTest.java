package br.com.caelum.main;

public class StringTest {

	/* 13.7 EXERCÍCIOS: JAVA.LANG.STRING */

	public static void main(String[] args) {

		// Troca os caracteres de uma String
		String texto = "fj11";
		texto = texto.replaceAll("1", "2");

		System.out.println(texto);

		// Remove os espaços em branco das pontas de uma String
		String texto2 = "fj11 ";
		texto2 = texto2.trim();

		System.out.println(texto2);

		// Imprime cada caractere em uma linha diferente
		String texto3 = "Developer - Java";

		for (char c : texto3.toCharArray()) {
			System.out.println("Char: " + c);
		}

		// Imprime a String de trás para a frente e em uma linha só
		String texto4 = "Socorram-me, subi no ônibus em Marrocos";
		StringBuilder invertido = new StringBuilder(texto4).reverse();

		System.out.println(invertido);
	}

}
