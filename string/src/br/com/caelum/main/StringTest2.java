package br.com.caelum.main;

public class StringTest2 {

	/* 13.6 JAVA.LANG.STRING */

	public static void main(String[] args) {

		// Retorna o texto maiússculo
		String palavra = "fj-11";
		palavra = palavra.toUpperCase();
		System.out.println(palavra);

		// Retorna o texto minúsculo
		String palavra6 = "FJ-11";
		palavra6 = palavra6.toLowerCase();
		System.out.println(palavra6);

		// Substitui os caracteres
		String palavra2 = "fj-11";
		palavra2 = palavra2.replace("1", "2");
		System.out.println(palavra2);

		// Retorna o texto maiúsculo e substitui os caracteres
		String palavra3 = "fj-11";
		palavra3 = palavra3.toUpperCase().replace("1", "2");
		System.out.println(palavra3);

		// Retorna o caractere existente na posição i da String
		String palavra4 = "fj-11";
		char letra = palavra4.charAt(4);
		System.out.println(letra);

		// Retorna o caractere existente na posição i da String
		String palavra5 = "fj-11";
		int qtdLetras = palavra5.length();
		System.out.println(qtdLetras);

		// Devolve true se a String for vazia ou false caso contrário
		String palavra7 = "";
		boolean string = palavra7.isEmpty();
		System.out.println(string);

		// Devolve true se encontrar a String ou false caso contrário
		String palavra8 = "fj-11";
		boolean novaPalavra = palavra8.contains("fjj");
		System.out.println(novaPalavra);
	}

}
