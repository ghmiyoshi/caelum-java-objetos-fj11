package com.caelum.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.caelum.comparators.ComparadorPorTamanho;

public class ComparatorsTest {

	/* 15.17 PARA SABER MAIS: COMPARATORS, CLASSES ANÔNIMAS, JAVA 8 E O LAMBDA */

	public static void main(String[] args) {
		List<String> lista = new ArrayList<>();
		lista.add("Gabriel");
		lista.add("Kah");
		lista.add("Nadine");

		// invocando o sort passando o comparador
		ComparadorPorTamanho comparador = new ComparadorPorTamanho();

		// Collections.sort(lista, comparador);
		Collections.sort(lista, new ComparadorPorTamanho());

		System.out.println(lista);
	}

}
