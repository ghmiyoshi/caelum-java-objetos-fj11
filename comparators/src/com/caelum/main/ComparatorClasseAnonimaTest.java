package com.caelum.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorClasseAnonimaTest {

	/* 15.17 PARA SABER MAIS: COMPARATORS, CLASSES ANÔNIMAS, JAVA 8 E O LAMBDA */

	public static void main(String[] args) {
		List<String> lista = new ArrayList<>();

		lista.add("Gabriel");
		lista.add("Kah");
		lista.add("Nadine");

//		Comparator<String> comparador = new Comparator<String>() {
//			public int compare(String s1, String s2) {
//				return Integer.compare(s1.length(), s2.length());
//			}
//		};
//		
//		Collections.sort(lista, comparador);
//		System.out.println(lista);

//		Collections.sort(lista, new Comparator<String>() {
//			public int compare(String s1, String s2) {
//				return Integer.compare(s1.length(), s2.length());
//			}
//		});

		Collections.sort(lista, (s1, s2) -> Integer.compare(s1.length(), s2.length()));

		System.out.println(lista);
	}

}
