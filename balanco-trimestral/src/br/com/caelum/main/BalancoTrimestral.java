package br.com.caelum.main;

public class BalancoTrimestral {

	/* 3.3 EXERCÍCIOS: VARIÁVEIS E TIPOS PRIMITIVOS */

	public static void main(String[] args) {
		int gastosJaneiro = 15000; // declarando variável (gastosJaneiro deve valer quinze mil)
		int gastosFevereiro = 23000;
		int gastosMarco = 17000;

		int gastosTrimestre = gastosJaneiro + gastosFevereiro + gastosMarco;
		int mediaMensal = gastosTrimestre / 3;

		System.out.println("Gastos no trimestre: R$ " + gastosTrimestre);
		System.out.println("Valor da média mensal: R$ " + mediaMensal);
	}

}
