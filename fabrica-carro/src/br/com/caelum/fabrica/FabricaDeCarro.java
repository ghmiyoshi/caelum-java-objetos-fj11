package br.com.caelum.fabrica;

public class FabricaDeCarro {

	private static FabricaDeCarro unicaInstancia = new FabricaDeCarro();

	private FabricaDeCarro() {

	}

	public static FabricaDeCarro getInstance() {
		return unicaInstancia;
	}

}
