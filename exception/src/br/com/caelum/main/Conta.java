package br.com.caelum.main;

import br.com.caelum.exception.SaldoInsuficienteException;
import br.com.caelum.modelo.ContaCorrente;

public class Conta {

	public static void main(String[] args) {
		ContaCorrente contaCorrente = new ContaCorrente();
		
		contaCorrente.deposita(0);
		
		try {
			contaCorrente.saca(10);
		} catch (SaldoInsuficienteException erro) {
			System.out.println(erro.getMessage());
		}

		System.out.println(contaCorrente.getSaldo());
	}

}
