package br.com.caelum.main;

import br.com.caelum.modelo.ContaCorrente;

public class Exception {

	/* 12.11 EXERCÍCIOS: EXCEÇÕES */

	public static void main(String[] args) {
		System.out.println("inicio do main");
		metodo1();
		System.out.println("fim do main");
	}

	static void metodo1() {
		System.out.println("inicio do metodo1");
		metodo2();
		System.out.println("fim do metodo1");
	}

	static void metodo2() {
		System.out.println("inicio do metodo2");
		ContaCorrente cc = new ContaCorrente();

		try { 
			for (int i = 0; i <= 15; i++) {
				cc.deposita(i + 1000);
				System.out.println(cc.getSaldo());
				if (i == 5) {
					cc = null;
				}
			}
		} catch (NullPointerException erro) { // a partir do momento que uma exception foi catched (pega, tratada, handled), a execução volta ao normal a partir daquele ponto.
			System.out.println("Erro: " + erro);
		}

		System.out.println("fim do metodo2");
	}

}
