package br.com.caelum.exception;

public class SaldoInsuficienteException extends RuntimeException {

	// criando meu própio tipo de exceção
	public SaldoInsuficienteException(String mensagem) {
		super(mensagem);
	}

}
