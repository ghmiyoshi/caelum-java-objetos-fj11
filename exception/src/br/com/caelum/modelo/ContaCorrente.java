package br.com.caelum.modelo;

import br.com.caelum.exception.SaldoInsuficienteException;

public class ContaCorrente {

	private double saldo;

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public void deposita(double valor) {
		this.saldo += valor;
	}

	public void saca(double valor) {
		if (this.saldo < valor) {
			throw new SaldoInsuficienteException("Saldo insuficiente");
		} else {
			this.saldo -= valor;
		}
	}

}
