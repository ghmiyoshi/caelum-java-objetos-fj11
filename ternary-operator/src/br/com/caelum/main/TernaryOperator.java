package br.com.caelum.main;

public class TernaryOperator {
	
	/*  4.13 DESAFIOS */
	
	public static void main(String[] args) {
		int a = 1;
		int b = 0;

		/*
		 * if (a > 0) { 
		 * 	  b = 1; 
		 *    System.out.println(b);
		 * } else { 
		 *    b = 2;
		 * }
		 * 
		 */

		b = (a > 0) ? 1 : 2; // Operador condicional tern�rio
		System.out.println(b);
	}

}
