package br.com.caelum.main.pessoa;

public class PessoaTest {

	/* 4.14 FIXANDO O CONHECIMENTO */

	public static void main(String[] args) {
		Pessoa pessoa = new Pessoa();

		pessoa.nome = "Gabriel";
		pessoa.idade = 22;
		pessoa.fazAniversario();

		System.out.println("Nome: " + pessoa.nome);
		System.out.println("Idade: " + pessoa.idade + " anos");
	}

}
