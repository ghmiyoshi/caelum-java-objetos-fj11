package br.com.caelum.main.porta;

public class Porta {

	boolean aberta;
	String cor;
	int dimensaoX;
	int dimensaoY;
	int dimensaoZ;

	void abre() {
		this.aberta = true;
	}

	void fecha() {
		this.aberta = false;
	}

	void pinta(String cor) {
		this.cor = cor;
	}

	boolean estaAberta() {
		if (this.aberta == true) {
			System.out.println("Está aberta!");
			return true;
		} else {
			System.out.println("Está fechada!");
			return false;
		}
	}

}
