package br.com.caelum.main.porta;

public class PortaTest {

	/* 4.14 FIXANDO O CONHECIMENTO */

	public static void main(String[] args) {
		Porta porta = new Porta();

		porta.abre();
		porta.fecha();
		porta.pinta("Azul");
		porta.dimensaoX = 10;
		porta.dimensaoY = 20;
		porta.dimensaoZ = 30;

		porta.estaAberta();
		System.out.println("Cor: " + porta.cor);
		System.out.println("Dimensão X: " + porta.dimensaoX);
		System.out.println("Dimensão Y: " + porta.dimensaoY);
		System.out.println("Dimensão Z: " + porta.dimensaoZ);
	}

}
