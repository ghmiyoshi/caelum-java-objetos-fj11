package br.com.caelum.main.casa;

public class Casa {

	String cor;
	boolean porta1;
	boolean porta2;
	boolean porta3;

	void pinta(String cor) {
		this.cor = cor;
	}

	int quantasPortasEstaoAbertas() {
		int quantidade = 0;
		if (this.porta1 == true) {
			quantidade += 1;
		}
		if (this.porta2 == true) {
			quantidade += 1;
		}

		if (this.porta3 == true) {
			quantidade += 1;
		}

		return quantidade;
	}

}
