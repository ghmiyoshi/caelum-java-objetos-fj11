package br.com.caelum.main.casa;

public class CasaTest {

	/* 4.14 FIXANDO O CONHECIMENTO */

	public static void main(String[] args) {
		Casa casa = new Casa();

		casa.porta1 = true;
		casa.porta2 = false;
		casa.porta3 = true;
		casa.pinta("Azul");

		System.out.println("Número de portas abertas: " + casa.quantasPortasEstaoAbertas());
		System.out.println("Cor: " + casa.cor);
	}

}
