package br.com.caelum.main;

import br.com.caelum.modelo.Banco;
import br.com.caelum.modelo.Conta;

public class BancoTest {

	/* 14.5 EXERCÍCIOS: ARRAYS */

	public static void main(String[] args) {
		Banco banco = new Banco("CaelumBank", 999);

		Conta[] contas = banco.getContas();

		for (int i = 0; i < 5; i++) {
			Conta conta = new Conta();
			banco.adiciona(conta, i);
			contas[i].deposita(i * 1000);
		}

		banco.mostraContas();
	}

}
