package br.com.caelum.main;

import br.com.caelum.modelo.Conta;

public class Array {

	/* 14.5 EXERCÍCIOS: ARRAYS */

	public static void main(String[] args) {
		Conta[] contas = new Conta[10];

		for (int i = 0; i < 10; i++) {
			contas[i] = new Conta();
			contas[i].deposita(i * 10);
			System.out.println(contas[i]);
		}

		// imprimindo toda a array usando enhanced-for
		for (Conta conta : contas) {
			System.out.println(conta.getSaldo());
		}
	}

}
