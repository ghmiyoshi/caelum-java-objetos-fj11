package br.com.caelum.modelo;

public class Banco {

	private String nome;
	private int numero;
	private Conta[] contas;

	public Banco(String nome, int numero) {
		this.nome = nome;
		this.numero = numero;
		this.contas = new Conta[10];
	}

	public String getNome() {
		return nome;
	}

	public int getNumero() {
		return numero;
	}

	public Conta[] getContas() {
		return contas;
	}

	public Conta pegaConta(int x) {
		return contas[x];
	}

	public void mostraContas() {
		try {
			for (Conta conta : contas) {
				System.out.println(conta.getSaldo());
			}
		} catch (RuntimeException erro) {
			System.out.println("Fim do array");
		}
	}

	public void adiciona(Conta conta, int i) {
		this.contas[i] = conta;
	}
}
