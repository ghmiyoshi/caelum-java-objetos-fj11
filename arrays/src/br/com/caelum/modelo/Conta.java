package br.com.caelum.modelo;

public class Conta {

	private int saldo;

	public void deposita(int valor) {
		this.saldo += valor;
	}

	public int getSaldo() {
		return saldo;
	}

}
