package br.com.caelum.fibonacci;

public class Fibonacci {
	
	/* Fn = Fn-1 + Fn - 2
	 
	   Exemplo:
	   Fn6 = Fn5 + Fn4 ou seja Fn5 = 5 + 3 
	 */
	
	public int calculaFibonacci(int i) {
		return ((i == 0 || i == 1) ? i : (calculaFibonacci (i - 1) + calculaFibonacci (i - 2)));
	}

}
