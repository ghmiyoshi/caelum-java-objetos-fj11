package br.com.caelum.main;

import br.com.caelum.fibonacci.Fibonacci;

public class FibonacciTest {

	/* 4.13 DESAFIOS */

	public static void main(String[] args) {
		Fibonacci fibonacci = new Fibonacci();

		for (int i = 1; i <= 10; i++) {
			int resultado = fibonacci.calculaFibonacci(i);
			System.out.println(resultado);
		}
	}

}
