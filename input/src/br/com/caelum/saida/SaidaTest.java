package br.com.caelum.saida;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class SaidaTest {

	/* 16.5 A ANALOGIA PARA A ESCRITA: OUTPUTSTREAM */

	public static void main(String[] args) throws IOException {
		OutputStream os = new FileOutputStream("saida.txt");
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter bw = new BufferedWriter(osw);

		bw.write("caelum");
		bw.close();
	}

}
