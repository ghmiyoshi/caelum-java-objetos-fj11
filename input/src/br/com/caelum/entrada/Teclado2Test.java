package br.com.caelum.entrada;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class Teclado2Test {

	/* 16.6 UMA MANEIRA MAIS FÁCIL: SCANNER E PRINTSTREAM */

	public static void main(String[] args) throws FileNotFoundException {
		// lê do teclado com saída para um arquivo
		Scanner s = new Scanner(System.in);
		PrintStream ps = new PrintStream("arquivo.txt");

		while (s.hasNextLine()) {
			ps.println(s.nextLine());
		}
	}

}
