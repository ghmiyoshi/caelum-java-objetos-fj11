package br.com.caelum.entrada;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TecladoTest {

	/* 16.4 LENDO STRINGS DO TECLADO */

	public static void main(String[] args) throws IOException {
		// lê do teclado em vez de um arquivo
		InputStream is = System.in;

		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);

		String s = br.readLine();

		while (s != null) {
			System.out.println(s);
			s = br.readLine();
		}
	}

}
