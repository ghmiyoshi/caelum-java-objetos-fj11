package br.com.caelum.main.While;

public class Exercicio2 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {
		int soma = 0;
		int i = 0;

		while (i <= 1000) {
			soma += i;
			i++;
		}

		System.out.println(soma);
	}

}
