package br.com.caelum.main.While;

public class Exercicio1 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {
		int i = 150;

		while (i <= 300) {
			System.out.println(i);
			i++;
		}
	}

}
