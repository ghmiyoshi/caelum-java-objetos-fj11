package br.com.caelum.main.While;

public class Exercicio3 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */
	
	public static void main(String[] args) {
		int i = 0;

		while (i < 100) {
			if (i % 3 == 0) {
				System.out.println(i);
			}
			i++;
		}
	}

}
