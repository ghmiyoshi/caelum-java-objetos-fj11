package br.com.caelum.main.While;

public class Exercicio7 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {
		int n = 13;
		int x = 13;

		while (n > 1) {
			if (n % 2 == 0) {
				n /= 2;
			} else {
				n = 3 * n + 1;
			}
			x--;

			System.out.println(n);
		}
	}

}
