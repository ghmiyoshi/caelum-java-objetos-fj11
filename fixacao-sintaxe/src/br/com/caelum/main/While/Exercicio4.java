package br.com.caelum.main.While;

public class Exercicio4 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {

		int fatorial = 1;
		int n = 1;

		while (n <= 10) {
			fatorial *= n;
			System.out.println(fatorial);
			n++;
		}
	}

}
