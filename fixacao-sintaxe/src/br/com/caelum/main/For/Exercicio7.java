package br.com.caelum.main.For;

public class Exercicio7 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {
		int n = 13;

		for (int x = 13; n > 1; x--) {
			if (n % 2 == 0) {
				n /= 2;
			} else {
				n = 3 * n + 1;
			}

			System.out.println(n);
		}
	}

}
