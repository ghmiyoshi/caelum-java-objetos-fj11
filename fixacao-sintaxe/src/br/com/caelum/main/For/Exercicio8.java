package br.com.caelum.main.For;

public class Exercicio8 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {
		for (int i = 1; i <= 4; i++) {
			for (int x = 1; x <= i; x++) {
				System.out.print(i * x + " ");
			}
			System.out.println();
		}
	}

}
