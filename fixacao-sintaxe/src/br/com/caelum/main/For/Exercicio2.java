package br.com.caelum.main.For;

public class Exercicio2 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {
		int soma = 0;

		for (int i = 0; i <= 1000; i++) {
			soma += i;
		}

		System.out.println(soma);
	}

}
