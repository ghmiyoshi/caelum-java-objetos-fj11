package br.com.caelum.main.For;

public class Exercicio4 {

	/* 3.13 EXERCÍCIOS: FIXAÇÃO DE SINTAXE */

	public static void main(String[] args) {
		int fatorial = 1;

		for (int n = 1; n <= 10; n++) {
			fatorial *= n;
			System.out.println(fatorial);
		}
	}

}
