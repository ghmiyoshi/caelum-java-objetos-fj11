package br.com.caelum.main;

import br.com.caelum.conta.Conta;
import br.com.caelum.conta.Data;

public class ContaTest {

	/* 4.12 EXERCÍCIOS: ORIENTAÇÃO A OBJETOS */

	public static void main(String[] args) {
		Conta conta = new Conta("Gabriel");
		Data data = new Data();

		data.setDia(30);
		data.setMes(2);
		data.setAno(2012);

		conta.setNumero(123);
		conta.setAgencia("45678-9");
		conta.setDataAbertura(data);
		conta.deposita(100.0);

		System.out.println("Saldo atual: R$ " + conta.getSaldo());
		System.out.println("Rendimento mensal: R$ " + conta.calculaRendimento());
		System.out.println(conta.recuperaDadosParaImpressao());
		System.out.println("Identificador: " + conta.getIdentificador());

		Conta conta2 = new Conta("Hideki");

		conta2.setNumero(123);
		conta2.setAgencia("12345-6");
		conta2.setDataAbertura(data);
		conta2.deposita(200.0);

		System.out.println("Saldo atual: R$ " + conta2.getSaldo());
		System.out.println("Rendimento mensal: R$ " + conta2.calculaRendimento());
		System.out.println(conta2.recuperaDadosParaImpressao());
		System.out.println("Identificador: " + conta2.getIdentificador());

		if (conta == conta2) {
			System.out.println("São iguais");
		} else {
			System.out.println("São diferentes");
		}
	}

}
