package br.com.caelum.conta;

public class Conta {

	private String titular;
	private int numero;
	private String agencia;
	private double saldo;
	private Data dataAbertura;
	private static int identificador;

	public Conta() {
		Conta.identificador += 1;
	}

	public Conta(String titular) {
		this.titular = titular;
		Conta.identificador += 1;
	}

	public int getIdentificador() {
		return Conta.identificador;
	}

	public String getTitular() {
		return titular;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public Data getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Data dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public double getSaldo() {
		return saldo;
	}

	public void saca(double valor) {
		this.saldo -= valor;
	}

	public void deposita(double valor) {
		this.saldo += valor;
	}

	public double calculaRendimento() {
		return this.saldo *= 0.1;
	}

	public String recuperaDadosParaImpressao() {
		if (this.dataAbertura.getDia() == 31 && this.dataAbertura.getMes() == 2 && this.dataAbertura.getAno() == 2012) {
			return "Essa data não é permitida!";
		}

		return "Titular: " + this.titular + "\nNúmero: " + this.numero + "\nAgência: " + this.agencia + "\nSaldo: R$ "
				+ this.saldo + "\nData de abertura: " + this.dataAbertura.formatada();
	}

}
