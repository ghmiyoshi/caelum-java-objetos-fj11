package br.com.caelum.sockets;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Servidor {

	/* 19.8 EXERCÍCIOS: SOCKETS */

	public static void main(String[] args) throws IOException {
		ServerSocket servidor = new ServerSocket(12345);

		System.out.println("Porta 12345 aberta!");

		Socket cliente = servidor.accept();

		System.out.println("Nova conexãoo com o cliente " + cliente.getInetAddress().getHostAddress());

		Scanner teclado = new Scanner(cliente.getInputStream());

		while (teclado.hasNextLine()) {
			System.out.println(teclado.nextLine());
		}

		teclado.close();
		servidor.close();
		cliente.close();
	}

}
