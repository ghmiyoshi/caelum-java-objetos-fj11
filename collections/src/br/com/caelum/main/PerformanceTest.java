package br.com.caelum.main;

import java.util.Collection;
import java.util.HashSet;

public class PerformanceTest {

	/* 15.15 EXERCÍCIOS: COLLECTIONS */

	public static void main(String[] args) {
		System.out.println("Iniciando...");

		Collection<Integer> teste = new HashSet<>();
		// Collection<Integer> teste = new ArrayList<>();

		long inicio = System.currentTimeMillis();

		int total = 3000;

		for (int i = 0; i < total; i++) {
			teste.add(i);
		}

		long fimAdd = System.currentTimeMillis();
		long tempoAdd = fimAdd - inicio;

		System.out.println("Tempo gasto para add: " + tempoAdd);

		for (int i = 0; i < total; i++) {
			teste.contains(i);
		}

		long fim = System.currentTimeMillis();
		long tempo = fim - inicio;

		System.out.println("Tempo gasto para procurar: " + tempo);
	}

}
