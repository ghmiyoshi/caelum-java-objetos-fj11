package br.com.caelum.main;

import java.util.HashMap;
import java.util.Map;

import br.com.caelum.modelo.Conta;

public class MapaTest {

	/* 15.15 EXERCÍCIOS: COLLECTIONS */

	public static void main(String[] args) {
		Conta conta1 = new Conta();
		conta1.setTitular("Gabriel");
		conta1.setAgencia("Bradesco");

		Conta conta2 = new Conta();
		conta2.setTitular("Kalliny");
		conta2.setAgencia("NuBank");

		// cria o mapa
		Map<Integer, Conta> mapaDeContas = new HashMap<>();

		// adiciona duas chaves e seus valores
		mapaDeContas.put(1, conta1);
		mapaDeContas.put(2, conta2);

		// qual a conta do Gabriel?
		Conta contaDoGabriel = mapaDeContas.get(1);
		System.out.println(contaDoGabriel.getAgencia());
	}

}
