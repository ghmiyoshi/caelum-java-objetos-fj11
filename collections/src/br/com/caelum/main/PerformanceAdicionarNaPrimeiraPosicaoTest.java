package br.com.caelum.main;

import java.util.LinkedList;
import java.util.List;

public class PerformanceAdicionarNaPrimeiraPosicaoTest {

	/* 15.15 EXERCÍCIOS: COLLECTIONS */

	public static void main(String[] args) {
		System.out.println("Iniciando...");

		long inicio = System.currentTimeMillis();

		// trocar depois por ArrayList
		List<Integer> teste = new LinkedList<>();

		for (int i = 0; i < 300000; i++) {
			teste.add(0, i);
		}

		long fimAdd = System.currentTimeMillis();
		double tempoAdd = (fimAdd - inicio) / 1000.0;

		System.out.println("Tempo gasto: " + tempoAdd);

		for (int i = 0; i < teste.size(); i++) {
			teste.get(i);
		}

		long fim = System.currentTimeMillis();
		double tempo = (fim - inicio) / 1000.0;

		System.out.println("Tempo gasto para procurar: " + tempo);
	}
}
