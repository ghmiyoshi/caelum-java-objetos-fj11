package br.com.caelum.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrayListTest {

	/* 15.16 DESAFIOS */

	public static void main(String[] args) {
		List<Integer> numeros = new ArrayList<>();

		// gera e add números até 1000
		for (int i = 0; i < 100; i++) {
			numeros.add(i);
		}

		// inverte a lista
		Collections.reverse(numeros);

		// exibe os números
		System.out.println(numeros);
	}

}
