package br.com.caelum.main;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetTest {

	/* 15.16 DESAFIOS */

	public static void main(String[] args) {
		Set<Integer> numeros = new TreeSet<Integer>(new Comparator<Integer>() {

			// Regra de comparação para ordenar os números em ordem decrescente
			public int compare(Integer s1, Integer s2) {
				if (s1.compareTo(s2) < 0)
					return +1;
				else if (s1.compareTo(s2) > 0)
					return -1;
				else
					return 0;
			}
		});

		// Gera e add números até 1000
		for (int i = 0; i < 100; i++) {
			numeros.add(i);
		}

		// Exibe os números
		System.out.println(numeros);
	}

}
