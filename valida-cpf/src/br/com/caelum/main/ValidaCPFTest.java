package br.com.caelum.main;

import java.util.Scanner;

import br.com.caelum.validar.ValidaCPF;

public class ValidaCPFTest {

	/* 5.8 EXERCÍCIOS: ENCAPSULAMENTO, CONSTRUTORES E STATIC */
	
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);

		System.out.print("Informe um CPF: ");
		String CPF = teclado.next();

		System.out.print("\nResultado: ");
		
		if (ValidaCPF.isCPF(CPF) == true) {
			System.out.println(ValidaCPF.imprimeCPF(CPF));
		} else {
			System.out.println("Erro, CPF inválido !!!");
		}
	}

}
