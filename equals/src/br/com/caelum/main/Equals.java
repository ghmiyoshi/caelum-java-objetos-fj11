package br.com.caelum.main;

public class Equals {

	/* 13.4 MÉTODOS DO JAVA.LANG.OBJECT: EQUALS E TOSTRING */

	public static void main(String[] args) {
		String x = new String("fj11");
		String y = new String("fj11");

		if (x == y) {
			System.out.println("Referência para o mesmo objeto");
		} else {
			System.out.println("Referências para objetos diferentes!");
		}

		if (x.equals(y)) {
			System.out.println("Consideramos iguais no critério de igualdade");
		} else {
			System.out.println("Consideramos diferentes no critério de igualdade");
		}
	}

}
